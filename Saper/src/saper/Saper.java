package saper;

import saper.controller.Controller;
import saper.model.Parameters;
import saper.model.Parameters.Difficulty;

/**
 * @author Pawe� Kaczy�ski
 *         Main game class. Initialize the game and holds the game controller.
 */
public class Saper
{
	/**
	 * Project version.
	 */
	public final static String VERSION = "1.1.0";

	/**
	 * MVC's controller for Saper project.
	 * 
	 * @see saper.controller.Controller
	 */
	private static Controller controller;

	/**
	 * Start of the game.
	 * 
	 * @param args
	 *            Main method arguments.
	 */
	public static void main (String[] args)
	{
		// Load difficulty preset.
		if (Parameters.setParameters(Difficulty.EASY))
			controller = new Controller();

		// Start the game.
		controller.run();
	}
}
