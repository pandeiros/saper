package saper.events;

import saper.controller.Controller;
import saper.view.View.PanelTypes;

/**
 * @author Pawe� Kaczy�ski
 *         Specifies different window opening and closing actions and options changing.
 */
public final class InterfaceEvent extends SaperEvent
{
    /**
     * Contains of all available interface event actions.
     */
    public static enum Type
    {
        /**
         * Event type for showing 'About' window.
         */
        ABOUT_SHOW,

        /**
         * Event type for exiting the game. Event triggered by controller or via game menu.
         */
        EXIT,

        /**
         * Event type for showing 'Stats' panel.
         */
        HELP_HIDE,

        /**
         * Event type for hiding 'Stats' panel.
         */
        HELP_OPEN,

        /**
         * Event type for starting new game. Event triggered by controller or via game menu.
         */
        NEW_GAME,

        /**
         * Event type for showing 'Options' panel.
         */
        OPTIONS_HIDE,

        /**
         * Event type for hiding 'Options' panel.
         */
        OPTIONS_SHOW,

        /**
         * Event type for showing 'Stats' panel.
         */
        STATS_HIDE,

        /**
         * Event type for hiding 'Stats' panel.
         */
        STATS_SHOW;
    }

    /**
     * Constructor with event type.
     * 
     * @param type
     *            Type of created event.
     * @see saper.events.InterfaceEvent.Type
     */
    public InterfaceEvent (Type type)
    {
        this.type = type;
    }

    /*
     * (non-Javadoc)
     * @see saper.events.SaperEvent#handleEvent(saper.controller.Controller)
     */
    @Override
    public void handleEvent (Controller controller)
    {
        // Check for event type.
        switch (type)
        {
            case NEW_GAME:
                controller.newGame(false);
                controller.getView().managePanels(PanelTypes.BOARD, true);
                break;
            case OPTIONS_HIDE:
                controller.getView().managePanels(PanelTypes.OPTIONS, false);
                break;
            case OPTIONS_SHOW:
                controller.getView().managePanels(PanelTypes.OPTIONS, true);
                break;
            case STATS_HIDE:
                controller.getView().managePanels(PanelTypes.STATS, false);
                break;
            case STATS_SHOW:
                controller.getView().managePanels(PanelTypes.STATS, true);
                break;
            case HELP_HIDE:
                controller.getView().managePanels(PanelTypes.HELP, false);
                break;
            case HELP_OPEN:
                controller.getView().managePanels(PanelTypes.HELP, true);
                break;
            case ABOUT_SHOW:
                controller.getView().managePanels(PanelTypes.ABOUT, true);
                break;
            case EXIT:
                // If save needed (not new game).
                if (controller.getModel().getStatus() && controller.getModel().getFieldsRevealed() > 0)
                    controller.saveModel();

                // Save statistics and exit.
                controller.saveStats();
                controller.getView().close();
                break;
            default:
                break;
        }
    }

    /**
     * Type of this event.
     */
    private final Type type;
}
