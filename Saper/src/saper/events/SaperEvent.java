package saper.events;

import saper.controller.Controller;

/**
 * @author Pawe� Kaczy�ski
 *         Base abstract class for all game events.
 */
public abstract class SaperEvent
{
    /**
     * Abstract method for event handling. Any subclasses override this method.
     * 
     * @param controller
     *            Game controller to be passed for future game management.
     */
    public abstract void handleEvent (Controller controller);
}
