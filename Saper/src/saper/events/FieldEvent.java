package saper.events;

import saper.controller.Controller;
import saper.model.Parameters;

/**
 * @author Pawe� Kaczy�ski
 *         Class responsible for handling field pressing and flagging.
 */
public class FieldEvent extends SaperEvent
{
    /**
     * Constructor with field coordinates and mouse button flag.
     * 
     * @param position
     *            x-coordinate * maxHeight + y-coordinate.
     * @param leftClick
     *            True for left mouse button pressed, false for right mouse button.
     */
    public FieldEvent (final int position, final boolean leftClick)
    {
        this.position = position;
        this.leftClick = leftClick;
    }

    /*
     * (non-Javadoc)
     * @see saper.events.SaperEvent#handleEvent(saper.controller.Controller)
     */
    @Override
    public void handleEvent (Controller controller)
    {
        // Acquire coordinates.
        final int x = position / Parameters.getHeight();
        final int y = position % Parameters.getHeight();

        // Revealing.
        if (leftClick)
        {
            // Initiate board on click.
            if (!controller.getModel().getInitialized())
                controller.getModel().initiateBoard(x, y);

            // Reveal recursively starting with pressed field.
            controller.getModel().revealPressedField(x, y);
        }
        // Flagging.
        else
        {
            try
            {
                // Try unflagging the field.
                if (controller.getModel().getBoard()[x][y].getFlagged())
                {
                    controller.getModel().getBoard()[x][y].setFlagged(false);

                    // Update mines left counter.
                    controller.getModel().updateMinesLeft(true);
                }
                // Try flagging the field.
                else
                {
                    controller.getModel().getBoard()[x][y].setFlagged(true);

                    // Update mines left counter.
                    controller.getModel().updateMinesLeft(false);

                    // Increment flag counter.
                    controller.getStats().incFlagSet();
                }
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                // DO NOTHING
            }
        }
    }

    /**
     * Mouse button indicator. True for left mouse click, false for right mouse click.
     */
    private final boolean leftClick;

    /**
     * Represents x-coordinate * maxHeight + y-coordinate (unique button position).
     */
    private final int position;
}
