package saper.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import saper.events.SaperEvent;
import saper.model.GameStats;
import saper.model.Model;
import saper.model.Parameters;
import saper.view.View;

/**
 * MVC's controller for Saper project. Continously check for events in the
 * BlockingQueue and execute them. Controls game result and asks for restart as well
 * as finalizes the game.
 * 
 * @author Pawe� Kaczy�ski
 */
public class Controller
{
    /**
     * Constructor with all needed game components.
     */
    public Controller ()
    {
        this.eventQueue = new LinkedBlockingQueue<SaperEvent>();

        // If save file with model is available, load it. Otherwise create new one.
        if (!loadModel())
            this.model = new Model();

        // Create new View. Draw board if model was already initialized.
        this.view = new View(eventQueue);
        if (model.getInitialized())
            this.view.draw(model.getBoard(), model.getMinesLeft());

        // If save file with stats is available, loas it. Otherwise create new Stats object.
        if (!loadStats())
            this.stats = new GameStats();

        // Pass data from stats to the view.
        this.view.updateStats(stats);
    }

    /**
     * Model getter.
     * 
     * @return Model associated with this controller.
     * @see saper.model.Model
     */
    public final Model getModel ()
    {
        return model;
    }

    /**
     * Stats getter.
     * 
     * @return Stats associated with this controller.
     * @see saper.model.GameStats
     */
    public final GameStats getStats ()
    {
        return stats;
    }

    /**
     * View getter.
     * 
     * @return View associated with this controller.
     * @see saper.view.View
     */
    public final View getView ()
    {
        return view;
    }

    /**
     * Checks for file "model.sav" and loads model from it if possible.
     * 
     * @return True for succesful load, false otherwise.
     */
    public final boolean loadModel ()
    {
        try
        {
            // Try to read object from file.
            FileInputStream fileStream = new FileInputStream("./model.sav");
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            model = (Model) objectStream.readObject();

            // Update game parameters based on loaded model.
            Parameters.Difficulty diff = model.getDifficulty();
            int width = model.getBoard().length;
            int height = model.getBoard()[0].length;
            int mines = model.getMines();
            Parameters.setParameters(diff, width, height, mines);

            objectStream.close();

            // Delete save file (cause it's just temporary).
            File file = new File("./model.sav");
            file.delete();
        }
        catch (FileNotFoundException e)
        {
            // DO NOTHING Nothing to load, so leave it there.
            return false;
        }
        catch (IOException e)
        {
            JOptionPane.showMessageDialog(null, "Loading model from file failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Critical error!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
            view.close();
            return false;
        }

        return true;
    }

    /**
     * Checks for file 'stats.sav' and loads game statistics form it.
     * 
     * @return True for successful load, false otherwise.
     */
    public final boolean loadStats ()
    {
        try
        {
            // Try to load stats from file.
            FileInputStream fileStream = new FileInputStream("./stats.sav");
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            stats = (GameStats) objectStream.readObject();
            objectStream.close();

        }
        catch (FileNotFoundException e)
        {
            // DO NOTHING Nothing to load, so leave it there.
            return false;
        }
        catch (IOException e)
        {
            JOptionPane.showMessageDialog(null, "Loading stats from file failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Critical error!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
            view.close();
            return false;
        }

        return true;
    }

    /**
     * Starts new game (creates new Model and updates View to prepare for new game).
     * 
     * @param newGame
     *            If true, game will definetely start. If false, controller checks for additional
     *            conditions and/or asks for confirmation.
     */
    public final void newGame (final boolean newGame)
    {
        // Tells whether game should restart.
        boolean start = newGame;

        // If Parameters' values were reset.
        if (Parameters.getReset())
        {
            start = true;
            Parameters.setReset(false);
        }
        // If at least one field was clicked.
        else if (model.getFieldsRevealed() != 0)
        {
            // If game ended in defeat.
            if (model.getMineRevealed())
                start = true;
            else
            {
                // Otherwise, prepare confirmation dialog.
                String message = "Start new game?\nAll current game progress will be lost.";

                // Ask for game restart.
                int choice =
                        JOptionPane.showConfirmDialog(new JButton(), message, "Game over!", JOptionPane.YES_NO_OPTION);

                if (choice == JOptionPane.YES_OPTION)
                    start = true;
            }
        }

        // Restart or not?
        if (start)
        {
            view.newGame();
            model = new Model();
        }
    }

    /**
     * Continously polls game for events, executes them and updates the view.
     */
    public final void run ()
    {
        while (true)
        {
            // Check if game is over.
            while (model.getStatus())
            {
                // Poll for events
                while (!eventQueue.isEmpty())
                {
                    SaperEvent event = eventQueue.poll();
                    event.handleEvent(this);

                    if (model.getInitialized())
                        view.draw(model.getBoard(), model.getMinesLeft());

                    // Update GameStats
                    stats.incFieldRevealed(model.getPendingFieldsRevealed());
                    view.updateStats(stats);
                }

            }

            // Check for game result and print it.
            String message = "";
            if (model.getMineRevealed())
            {
                message = "KABOOM! You lose :(";
                stats.incGames();
            }
            else
            {
                message = "YEAH! You win :D";
                stats.incWins();
                stats.incMinesSecured();
                model.setFieldsRevealed(0);
            }

            // Display result.
            int timeResult = view.endGame();
            message += "\n >> Your time was:  " + timeResult + " second(s).";
            message += "\n\nWanna play another game? ;)";

            // Ask for game restart.
            int choice =
                    JOptionPane.showConfirmDialog(new JButton(), message, "Game over!", JOptionPane.YES_NO_OPTION);

            if (choice == JOptionPane.YES_OPTION)
                newGame(true);
            else
            {
                // If game is over, save stats and exit.
                saveStats();
                view.close();
                break;
            }
        }
    }

    /**
     * Saves model into 'model.sav' file. This method is called before exit.
     */
    public final void saveModel ()
    {
        try
        {
            // Try to save model into a file.
            FileOutputStream fileStream = new FileOutputStream("./model.sav");
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(model);
            objectStream.close();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(new JButton(), "Saving model to file failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Saves stats into 'stats.sav' file. This method is called before exit.
     */
    public final void saveStats ()
    {
        try
        {
            // Try to save stats into a file.
            FileOutputStream fileStream = new FileOutputStream("./stats.sav");
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(stats);
            objectStream.close();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(new JButton(), "Saving stats to file failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Container for queueing special game events.
     */
    private final BlockingQueue<SaperEvent> eventQueue;

    /**
     * MVC's model for Saper project.
     * 
     * @see saper.model.Model
     */
    private Model model;

    /**
     * Game statistics.
     * 
     * @see saper.model.GameStats
     */
    private GameStats stats;

    /**
     * MVC's view for Saper project.
     * 
     * @see saper.view.View
     */
    private final View view;

}
