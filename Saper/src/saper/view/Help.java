package saper.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Pawe� Kaczy�ski
 *         Implements "How to" info and controls.
 */
public class Help extends JPanel
{
	/**
	 * Help ID for serialization.
	 */
	private static final long serialVersionUID = 10L;

	/**
	 * Default constructor.
	 */
	public Help ()
	{
		// Set panel layout.
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		this.setBackground(new Color(204, 66, 7, 255));

		// Set help content.
		setLabels();
		setMargins();
		setHelpTitle();
	}

	/**
	 * Adds new label to the panel.
	 * 
	 * @param text
	 *            Label's text.
	 * @param cons
	 *            Contraints for placing Component on the panel.
	 * @param size
	 *            Font size.
	 * @param color
	 *            Font/Foreground color.
	 * @param bold
	 *            True for bold font, false for italic font.
	 */
	private final void addLabel (final String text, final GridBagConstraints cons, final float size, final Color color, final boolean bold)
	{
		Font font = new Font("Arial", bold ? Font.BOLD : Font.PLAIN, (int) size);
		JLabel label = new JLabel(text);
		label.setFont(font);
		label.setForeground(color);

		this.add(label, cons);
	}

	/**
	 * Sets all static labels showing different difficulty choices.
	 */
	private final void setLabels ()
	{
		Color labelColor = new Color(220, 220, 220, 255);

		GridBagConstraints cons = new GridBagConstraints();
		cons.weightx = 200;
		cons.weighty = 1;
		cons.gridx = 1;
		cons.gridy = 2;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.anchor = GridBagConstraints.WEST;
		cons.insets = new Insets(0, 15, 0, 15);

		addLabel("How to play?", cons, 15, labelColor, true);

		cons.insets = new Insets(0, 15, 15, 15);
		cons.gridy = 3;
		addLabel("<html>Click on a field to reveal part <br />of the board. Each field shows<br /> the number of adjacent mines. <br />" + "Your goal is to flag all mines by <br />right-cicking on an unrevealed field.<br /> If all mines are flagged and " + "other fields <br />are visible, the victory is Yours.</html>", cons, 14, labelColor, false);

		cons.insets = new Insets(0, 15, 0, 15);
		cons.gridy = 4;
		addLabel("Controls", cons, 15, labelColor, true);

		cons.insets = new Insets(0, 15, 15, 15);
		cons.gridy = 5;
		addLabel("<html>Left click: <b>Reveal a field</b><br />Right click: <b>Flag a mine<b/></html>", cons, 14, labelColor, false);
	}

	/**
	 * /**
	 * Sets additional margins to center the grid.
	 */
	private final void setMargins ()
	{
		Color anyColor = new Color(0, 0, 0, 255);

		GridBagConstraints cons = new GridBagConstraints();
		cons.weightx = 1000;
		cons.gridheight = 1;
		cons.gridx = 0;
		cons.gridy = 6;
		cons.gridwidth = 3;

		cons.weighty = 2000;
		addLabel("", cons, 11, anyColor, false);

		cons.gridy = 0;
		addLabel("", cons, 11, anyColor, false);

		cons.gridy = 1;
		cons.gridwidth = 1;
		cons.gridheight = 5;
		addLabel("", cons, 11, anyColor, false);

		cons.gridx = 2;
		addLabel("", cons, 11, anyColor, false);
	}

	/**
	 * Set title for this panel.
	 */
	private final void setHelpTitle ()
	{
		GridBagConstraints cons = new GridBagConstraints();
		cons.weightx = 200;
		cons.weighty = 1;
		cons.gridx = 1;
		cons.gridy = 1;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.anchor = GridBagConstraints.CENTER;
		cons.insets = new Insets(15, 0, 10, 0);

		Font font = new Font("Arial", Font.ITALIC, 30);
		JLabel label = new JLabel("Help");
		label.setFont(font);
		label.setForeground(new Color(200, 200, 200, 255));

		this.add(label, cons);
	}
}
