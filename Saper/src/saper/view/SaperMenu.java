package saper.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.concurrent.BlockingQueue;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import saper.events.InterfaceEvent;
import saper.events.SaperEvent;

/**
 * @author Pawe� Kaczy�ski
 *         Drop-down menu with available program operations (New game, Options, About, etc.).
 */
public class SaperMenu
{
    /**
     * Constructor with referenced blocking queue from View
     * 
     * @param blockingQueue
     *            Queue to be used for events handling.
     */
    public SaperMenu (final BlockingQueue<SaperEvent> blockingQueue)
    {
        // Create new menu
        menuBar = new JMenuBar();

        // Create two drop-down sections.
        menuBar.add(new JMenu("Game"));
        menuBar.add(new JMenu("Help"));

        // Create all clickable items.
        newGame = new JMenuItem("New game");
        options = new JMenuItem("Options");
        stats = new JMenuItem("Stats");
        exit = new JMenuItem("Save and exit");
        help = new JMenuItem("Help");
        about = new JMenuItem("About");

        // Set key bindings.
        newGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        options.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        stats.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
        help.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));
        about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));

        // Add item to sections.
        menuBar.getMenu(0).add(newGame);
        menuBar.getMenu(0).addSeparator();
        menuBar.getMenu(0).add(options);
        menuBar.getMenu(0).add(stats);
        menuBar.getMenu(0).addSeparator();
        menuBar.getMenu(0).add(exit);
        menuBar.getMenu(1).add(help);
        menuBar.getMenu(1).add(about);

        // Add listeners
        newGame.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent event)
            {
                blockingQueue.add(new InterfaceEvent(InterfaceEvent.Type.NEW_GAME));
            }
        });

        options.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent event)
            {
                blockingQueue.add(new InterfaceEvent(InterfaceEvent.Type.OPTIONS_SHOW));
            }
        });

        stats.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent event)
            {
                blockingQueue.add(new InterfaceEvent(InterfaceEvent.Type.STATS_SHOW));
            }
        });

        exit.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent event)
            {
                blockingQueue.add(new InterfaceEvent(InterfaceEvent.Type.EXIT));
            }
        });

        help.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent event)
            {
                blockingQueue.add(new InterfaceEvent(InterfaceEvent.Type.HELP_OPEN));
            }
        });

        about.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent event)
            {
                blockingQueue.add(new InterfaceEvent(InterfaceEvent.Type.ABOUT_SHOW));
            }
        });
    }

    /**
     * JMenuBar getter
     * 
     * @return Menu to be displayed on the frame.
     */
    public JMenuBar getMenuBar ()
    {
        return menuBar;
    }

    /**
     * Swing component with ready menu to be displayed.
     */
    private final JMenuBar menuBar;

    /**
     * File->New game menu item.
     */
    private final JMenuItem newGame;

    /**
     * File->Options menu item.
     */
    private final JMenuItem options;

    /**
     * File->Stats menu item.
     */
    private final JMenuItem stats;

    /**
     * File->Exit menu item.
     */
    private final JMenuItem exit;

    /**
     * Help->Help menu item.
     */
    private final JMenuItem help;

    /**
     * Help->About menu item.
     */
    private final JMenuItem about;
}
