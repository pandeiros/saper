package saper.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import saper.events.InterfaceEvent;
import saper.events.InterfaceEvent.Type;
import saper.events.SaperEvent;
import saper.model.Parameters;
import saper.model.Parameters.Difficulty;
import saper.model.Parameters.Errors;

/**
 * @author Pawe� Kaczy�ski
 *         Implements different radio buttons and text fields for specifiyng board properties.
 */
public class Options extends JPanel
{
    /**
     * Implements switching between fields when pressing Tab.
     */
    public static class TextFieldTraversalPolicy extends FocusTraversalPolicy
    {
        /**
         * Constructor with vector with Components for this policy to be set for.
         * 
         * @param order
         *            Vector containing components for setting policy.
         */
        public TextFieldTraversalPolicy (Vector<Component> order)
        {
            this.order = new Vector<Component>(order.size());
            this.order.addAll(order);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.FocusTraversalPolicy#getComponentAfter(java.awt.Container,
         * java.awt.Component)
         */
        @Override
        public Component getComponentAfter (Container focusCycleRoot, Component aComponent)
        {
            int idx = (order.indexOf(aComponent) + 1) % order.size();
            return order.get(idx);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.FocusTraversalPolicy#getComponentBefore(java.awt.Container,
         * java.awt.Component)
         */
        @Override
        public Component getComponentBefore (Container focusCycleRoot, Component aComponent)
        {
            int idx = order.indexOf(aComponent) - 1;
            if (idx < 0)
            {
                idx = order.size() - 1;
            }
            return order.get(idx);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.FocusTraversalPolicy#getDefaultComponent(java.awt.Container)
         */
        @Override
        public Component getDefaultComponent (Container focusCycleRoot)
        {
            return order.get(0);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.FocusTraversalPolicy#getFirstComponent(java.awt.Container)
         */
        @Override
        public Component getFirstComponent (Container focusCycleRoot)
        {
            return order.get(0);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.FocusTraversalPolicy#getLastComponent(java.awt.Container)
         */
        @Override
        public Component getLastComponent (Container focusCycleRoot)
        {
            return order.lastElement();
        }

        /**
         * Vector containing Components that this policy is set for.
         */
        Vector<Component> order;

    }

    /**
     * Implements events handling char insertion into a JTextField specifically.
     */
    private class CharInsertAction implements KeyListener
    {
        /*
         * (non-Javadoc)
         * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
         */
        @Override
        public void keyPressed (KeyEvent k)
        {
            // DONOTHING
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
         */
        @Override
        public void keyReleased (KeyEvent k)
        {
            // DONOTHING
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
         */
        @Override
        public void keyTyped (KeyEvent k)
        {
            // If non-digit character is about to be inserted, ignore it.
            if (!Character.isDigit(k.getKeyChar()))
                k.consume();
        }
    }

    /**
     * Implements events handling switching between different radio buttons representing game
     * difficulties.
     */
    private class DifficultyChangeAction implements ActionListener
    {
        /**
         * Constructor with difficulty type.
         * 
         * @param type
         *            Difficulty specified for this listener.
         */
        DifficultyChangeAction (final Difficulty type)
        {
            this.type = type;
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed (ActionEvent event)
        {
            // Enable text fields when CUSTOM radio button is chosen. Disable them otherwise.
            if (type == Difficulty.CUSTOM)
            {
                widthTF.setEnabled(true);
                heightTF.setEnabled(true);
                minesTF.setEnabled(true);
            }
            else
            {
                widthTF.setEnabled(false);
                heightTF.setEnabled(false);
                minesTF.setEnabled(false);
            }

            // Do not let change difficulty to already active one.
            if (type != Parameters.difficulty || type == Difficulty.CUSTOM)
                confirmButton.setEnabled(true);
            else
                confirmButton.setEnabled(false);
        }

        /**
         * Difficulty associated with this listener.
         */
        private final Difficulty type;

    }

    /**
     * Implements events handling options appliance.
     */
    private class OptionsApplyAction implements ActionListener
    {
        /**
         * Constructor with button specification.
         * 
         * @param isConfirm
         *            True for "Apply" button, false for "Cancel" button.
         */
        public OptionsApplyAction (final boolean isConfirm)
        {
            this.isConfirm = isConfirm;
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed (ActionEvent event)
        {
            try
            {
                // If given parameters are correct.
                boolean correct = false;

                if (isConfirm)
                {
                    String command = buttonGroup.getSelection().getActionCommand();
                    if (command.equals("CUSTOM"))
                    {
                        // Acquire custom selected board data.
                        int width = Integer.parseInt(widthTF.getText());
                        int height = Integer.parseInt(heightTF.getText());
                        int mines = Integer.parseInt(minesTF.getText());

                        // Check parameters.
                        final Errors result = Parameters.checkBoardData(width, height, mines);

                        // Optionally display error message.
                        if (result != Errors.BOARD_CORRECT)
                        {
                            String message = Parameters.getMessage(result);
                            JOptionPane.showMessageDialog(null, message, "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                        else
                        {
                            // Set parameters if they are correct.
                            Parameters.setParameters(Difficulty.CUSTOM, width, height, mines);
                            correct = true;
                        }
                    }
                    else
                    {
                        // Set default presets.
                        Parameters.setParameters(command);
                        correct = true;
                    }

                    if (correct)
                    {
                        // Parameters were reset. Start new game.
                        Parameters.setReset(true);
                        eventQueue.add(new InterfaceEvent(Type.NEW_GAME));
                    }
                }

                // Close panels, show board.
                if (isConfirm && correct || !isConfirm)
                    eventQueue.add(new InterfaceEvent(Type.OPTIONS_HIDE));
            }
            catch (NumberFormatException e)
            {
                JOptionPane.showMessageDialog(widthTF, "Incorrect board data:\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }

        /**
         * Specifies button. True for "Apply", false for "Cancel".
         */
        private final boolean isConfirm;
    }

    /**
     * Options ID for serialization.
     */
    private static final long serialVersionUID = 6L;

    /**
     * Constructor with blocking queue.
     * 
     * @param blockingQueue
     *            Queue referenced from View
     */
    public Options (final BlockingQueue<SaperEvent> blockingQueue)
    {
        // Set queue and panel.
        this.eventQueue = blockingQueue;
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        this.setBackground(new Color(10, 101, 155, 255));

        // Create new radio button group.
        buttonGroup = new ButtonGroup();

        // Set all components.
        setOptionsTitle();
        setOptionsRadioButtons();
        setOptionsLabels();
        setOptionsTextFields();
        setOptionsMargins();
        setOptionsConfirmButtons();

        // Set traversal policy (tab switching)
        Vector<Component> order = new Vector<Component>(3);
        order.add(widthTF);
        order.add(heightTF);
        order.add(minesTF);
        policy = new TextFieldTraversalPolicy(order);
    }

    /**
     * Creates label from given arguments ans adds it to the main panel.
     * 
     * @param text
     *            Label text.
     * @param cons
     *            Constraints for placing the event on the panel.
     * @param size
     *            Font size.
     * @param color
     *            Font color (foreground color).
     */
    private final void addLabel (final String text, final GridBagConstraints cons, final float size, final Color color)
    {
        // Create...
        JLabel label = new JLabel(text);
        label.setFont(label.getFont().deriveFont(size));
        label.setForeground(color);

        // ...and set.
        this.add(label, cons);
    }

    /**
     * Adds radio button specifiyng difficulty.
     * 
     * @param label
     *            Button's text.
     * @param difficulty
     *            Associated difficulty.
     */
    private final void addRadioButton (final String label, final Difficulty difficulty)
    {
        // New radio button.
        JRadioButton button = new JRadioButton(label, difficulty == Parameters.difficulty);

        // Prepare constraints.
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 40;
        cons.weighty = 10;
        cons.gridx = 1;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.WEST;
        cons.insets = new Insets(0, 5, 0, 0);

        // Set command for further button recognition.
        String command = "";
        switch (difficulty)
        {
            case EASY:
                cons.gridy = 2;
                command = "EASY";
                break;
            case MEDIUM:
                cons.gridy = 4;
                command = "MEDIUM";
                break;
            case HARD:
                cons.gridy = 6;
                command = "HARD";
                break;
            case CUSTOM:
                cons.gridx = 3;
                cons.gridy = 2;
                cons.insets = new Insets(0, 0, 0, 0);
                cons.gridwidth = 2;
                command = "CUSTOM";
                break;
        }

        // Set the rest of button's parameters.
        button.setActionCommand(command);
        button.addActionListener(new DifficultyChangeAction(difficulty));
        button.setBackground(new Color(10, 101, 155, 255));
        button.setForeground(new Color(160, 220, 255, 255));
        Font font = new Font("Arial", Font.BOLD | Font.ITALIC, 14);
        button.setFont(font);
        button.setFocusable(false);
        button.setFocusPainted(false);

        // Add this button to button group and panel.
        buttonGroup.add(button);
        this.add(button, cons);
    }

    /**
     * Sets "Apply" and "Cancel" buttons.
     */
    private final void setOptionsConfirmButtons ()
    {
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 100;
        cons.weighty = 10;
        cons.gridx = 1;
        cons.gridy = 9;
        cons.gridwidth = 2;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.CENTER;
        cons.insets = new Insets(0, 0, 0, 0);

        confirmButton = new JButton("Apply");
        confirmButton.setEnabled(false);
        confirmButton.addActionListener(new OptionsApplyAction(true));
        this.add(confirmButton, cons);

        cons.gridx = 3;
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new OptionsApplyAction(false));
        this.add(cancelButton, cons);
    }

    /**
     * Sets all static labels showing different difficulty choices.
     */
    private final void setOptionsLabels ()
    {
        Color labelColor = new Color(220, 220, 220, 255);
        Color minesColor = new Color(180, 180, 180, 255);
        Color rangeColor = new Color(200, 200, 200, 255);
        Color white = new Color(255, 255, 255, 255);

        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 40;
        cons.weighty = 1;
        cons.gridx = 2;
        cons.gridy = 2;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.WEST;
        cons.insets = new Insets(0, 5, 0, 0);

        addLabel("9 x 9", cons, 14, labelColor);

        cons.gridy = 4;
        addLabel("16 x 16", cons, 14, labelColor);

        cons.gridy = 6;
        addLabel("24 x 20", cons, 14, labelColor);

        cons.insets = new Insets(-10, 5, 10, 0);
        cons.gridy = 3;
        addLabel("10 mines", cons, 13, minesColor);

        cons.gridy = 5;
        addLabel("40 mines", cons, 13, minesColor);

        cons.gridy = 7;
        addLabel("100 mines", cons, 13, minesColor);

        cons.gridx = 3;
        cons.gridy = 3;
        cons.insets = new Insets(0, 15, 0, 5);
        addLabel("Width:", cons, 14, white);

        cons.gridy = 5;
        addLabel("Height:", cons, 14, white);

        cons.gridy = 7;
        addLabel("Mines:", cons, 14, white);

        cons.insets = new Insets(-5, 15, 10, 5);
        cons.gridy = 4;
        addLabel("(" + Parameters.MIN_SIZE + " - " + Parameters.MAX_WIDTH + ")", cons, 11, rangeColor);

        cons.gridy = 6;
        addLabel("(" + Parameters.MIN_SIZE + " - " + Parameters.MAX_HEIGHT + ")", cons, 11, rangeColor);

        cons.gridy = 8;
        addLabel("(" + Parameters.MIN_MINES + " - " + Parameters.MAX_MINES + ")", cons, 11, rangeColor);
    }

    /**
     * Sets additional margins to center the grid.
     */
    private final void setOptionsMargins ()
    {
        Color anyColor = new Color(0, 0, 0, 255);

        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 1000;
        cons.gridheight = 1;
        cons.gridx = 0;
        cons.gridy = 10;
        cons.gridwidth = 6;
        cons.weighty = 1000;

        cons.weighty = 2000;
        addLabel("", cons, 11, anyColor);

        cons.gridy = 0;
        addLabel("", cons, 11, anyColor);

        cons.gridy = 1;
        cons.gridwidth = 1;
        cons.gridheight = 8;
        addLabel("", cons, 11, anyColor);

        cons.gridx = 5;
        addLabel("", cons, 11, anyColor);
    }

    /**
     * Sets four radio buttons, one for each difficulty.
     */
    private final void setOptionsRadioButtons ()
    {
        addRadioButton("Easy", Difficulty.EASY);
        addRadioButton("Medium", Difficulty.MEDIUM);
        addRadioButton("Hard", Difficulty.HARD);
        addRadioButton("Custom", Difficulty.CUSTOM);
    }

    /**
     * Sets three text fields, one for each board custom parameter (width, height, mines).
     */
    private final void setOptionsTextFields ()
    {
        widthTF = new JTextField(5);
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 50;
        cons.weighty = 10;
        cons.gridx = 4;
        cons.gridy = 3;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.WEST;
        cons.insets = new Insets(0, 0, 0, 10);

        widthTF.addKeyListener(new CharInsertAction());
        this.add(widthTF, cons);

        cons.gridy = 5;
        heightTF = new JTextField(5);
        heightTF.addKeyListener(new CharInsertAction());
        this.add(heightTF, cons);

        cons.gridy = 7;
        minesTF = new JTextField(5);
        minesTF.addKeyListener(new CharInsertAction());
        this.add(minesTF, cons);

        if (Parameters.difficulty != Difficulty.CUSTOM)
        {
            widthTF.setEnabled(false);
            heightTF.setEnabled(false);
            minesTF.setEnabled(false);
        }
    }

    /**
     * Set title for this panel.
     */
    private final void setOptionsTitle ()
    {
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 200;
        cons.weighty = 1;
        cons.gridx = 1;
        cons.gridy = 1;
        cons.gridwidth = 4;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.CENTER;
        cons.insets = new Insets(15, 0, 10, 0);

        Font font = new Font("Arial", Font.ITALIC, 30);
        JLabel label = new JLabel("Options");
        label.setFont(font);
        label.setForeground(new Color(200, 200, 200, 255));

        this.add(label, cons);
    }

    /**
     * Radio button group.
     */
    private ButtonGroup buttonGroup;

    /**
     * "Cancel" button. Returns to the board.
     */
    private JButton cancelButton;

    /**
     * "Apply" button. Confirms the specified options.
     */
    private JButton confirmButton;

    /**
     * Queue for events, referenced from the View.
     */
    private final BlockingQueue<SaperEvent> eventQueue;

    /**
     * Text field for board height input.
     */
    private JTextField heightTF;

    /**
     * Text field for mines number input.
     */
    private JTextField minesTF;

    /**
     * Text field for board width input.
     */
    private JTextField widthTF;

    /**
     * Tab traversal policy for text fields.
     */
    TextFieldTraversalPolicy policy;
}
