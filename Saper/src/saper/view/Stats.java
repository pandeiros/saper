package saper.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import saper.model.GameStats;

/**
 * @author Pawel
 *         Implements displaying game overall statistics on a separate panel.
 */
public class Stats extends JPanel
{
    /**
     * Stats ID for serialization.
     */
    private static final long serialVersionUID = 7L;

    /**
     * Default constructor.
     */
    public Stats ()
    {
        // Set layout and background.
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        this.setBackground(new Color(136, 10, 155, 255));

        // Set all components.
        setLabels();
        setValues();
        setMargins();
        setSeparator();
    }

    /**
     * Update all labels from given data.
     * 
     * @param stats
     *            Passed stats with data to be shown.
     */
    public final void updateStats (GameStats stats)
    {
        losesLabel.setText("" + (stats.getGames() - stats.getWins()));
        winsLabel.setText("" + stats.getWins());
        winsPerGameLabel.setText("" + (stats.getGames() != 0 ? (stats.getWins() * 100 / stats.getGames())
                : 0) + "%");
        gamesLabel.setText("" + (stats.getGames()));
        fieldsRevealedLabel.setText("" + stats.getFieldsRevealed());
        flagSetLabel.setText("" + stats.getFlagSet());
        minesSecuredLabel.setText("" + stats.getMinesSecured());
    }

    /**
     * Adds new label to the panel.
     * 
     * @param text
     *            Label's text.
     * @param cons
     *            Contraints for placing Component on the panel.
     * @param size
     *            Font size.
     * @param color
     *            Font/Foreground color.
     * @param bold
     *            True for bold font, false for italic font.
     * @return Created JLabel to be inserted to the panel.
     */
    private final JLabel addLabel (final String text, final GridBagConstraints cons, final float size, final Color color, final boolean bold)
    {
        Font font = new Font("Arial", bold ? Font.BOLD : Font.ITALIC, (int) size);
        JLabel label = new JLabel(text);
        label.setFont(font);
        label.setForeground(color);

        this.add(label, cons);

        return label;
    }

    /**
     * Sets all labels displaying game stats.
     */
    private final void setLabels ()
    {
        Color titleColor = new Color(200, 200, 200, 255);
        Color labelColor = new Color(226, 169, 255, 255);
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 200;
        cons.weighty = 1;
        cons.gridx = 1;
        cons.gridy = 1;
        cons.gridwidth = 2;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.CENTER;
        cons.insets = new Insets(10, 10, 10, 10);

        addLabel("Game statistics", cons, 30, titleColor, false);

        cons.gridy = 2;
        cons.gridwidth = 1;
        cons.weightx = 40;
        cons.anchor = GridBagConstraints.WEST;
        cons.insets = new Insets(0, 10, 0, 0);
        addLabel("Games played", cons, 14, labelColor, true);

        cons.gridy = 3;
        addLabel("Wins", cons, 14, labelColor, true);

        cons.gridy = 4;
        addLabel("Loses", cons, 14, labelColor, true);

        cons.gridy = 5;
        addLabel("Wins per game", cons, 14, labelColor, true);

        cons.gridy = 7;
        addLabel("Fields revealed", cons, 14, labelColor, true);

        cons.gridy = 8;
        addLabel("Flag set", cons, 14, labelColor, true);

        cons.gridy = 9;
        addLabel("Mines secured", cons, 14, labelColor, true);
    }

    /**
     * Sets additional panel margins for centering the grid.
     */
    private final void setMargins ()
    {
        Color anyColor = new Color(0, 0, 0, 0);
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 1000;
        cons.weighty = 1000;
        cons.gridwidth = 4;
        cons.gridheight = 1;
        cons.gridx = 0;
        cons.gridy = 10;

        addLabel("", cons, 11, anyColor, false);

        cons.gridy = 0;
        addLabel("", cons, 11, anyColor, false);

        cons.gridy = 1;
        cons.gridwidth = 1;
        cons.gridheight = 8;
        addLabel("", cons, 11, anyColor, false);

        cons.gridx = 3;
        addLabel("", cons, 11, anyColor, false);
    }

    /**
     * Set the line icon as a decorative separator.
     */
    private final void setSeparator ()
    {
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 200;
        cons.weighty = 1;
        cons.gridx = 1;
        cons.gridy = 6;
        cons.gridwidth = 2;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.CENTER;
        cons.insets = new Insets(0, -10, 0, 0);

        JLabel label = new JLabel();
        label.setIcon(new ImageIcon("./media/line.png"));
        this.add(label, cons);
    }

    /**
     * Insert values into labels.
     */
    private final void setValues ()
    {
        Color valueColor = new Color(241, 216, 255, 255);
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = 50;
        cons.weighty = 1;
        cons.gridx = 2;
        cons.gridy = 2;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.WEST;
        cons.insets = new Insets(0, 30, 0, 0);

        gamesLabel = addLabel("0", cons, 14, valueColor, true);

        cons.gridy = 3;
        winsLabel = addLabel("0", cons, 14, valueColor, true);

        cons.gridy = 4;
        losesLabel = addLabel("0", cons, 14, valueColor, true);

        cons.gridy = 5;
        winsPerGameLabel = addLabel("0", cons, 14, valueColor, true);

        cons.gridy = 7;
        fieldsRevealedLabel = addLabel("0", cons, 14, valueColor, true);

        cons.gridy = 8;
        flagSetLabel = addLabel("0", cons, 14, valueColor, true);

        cons.gridy = 9;
        minesSecuredLabel = addLabel("0", cons, 14, valueColor, true);
    }

    /**
     * Label displaying overall fields revealed count.
     */
    private JLabel fieldsRevealedLabel;

    /**
     * Label displaying flag set count.
     */
    private JLabel flagSetLabel;

    /**
     * Label displaying all games count.
     */
    private JLabel gamesLabel;

    /**
     * Label displaying loses count.
     */
    private JLabel losesLabel;

    /**
     * Label displaying number of mines secured ever.
     */
    private JLabel minesSecuredLabel;

    /**
     * Label displaying wins count.
     */
    private JLabel winsLabel;

    /**
     * Label displaying wins per game percentage.
     */
    private JLabel winsPerGameLabel;
}
