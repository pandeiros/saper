package saper.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.BlockingQueue;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import saper.Saper;
import saper.events.SaperEvent;
import saper.model.Field;
import saper.model.GameStats;
import saper.model.Parameters;

/**
 * @author Pawe� Kaczy�ski
 *         MVC's View for project Saper. Draw board, buttons, timer and mines count. Implements menu
 *         with keyboard shortcuts and manages everything displayable in the window.
 */
public class View
{

	/**
	 * Contains all available panels/windows displayable throughout the game.
	 */
	public static enum PanelTypes
	{
		/**
		 * Panel type for About window.
		 */
		ABOUT,

		/**
		 * Panel type for Board class (panel).
		 */
		BOARD,

		/**
		 * Panel type for Help class (panel).
		 */
		HELP,

		/**
		 * Panel type for Options class (panel).
		 */
		OPTIONS,

		/**
		 * Panel type for Stats class (panel).
		 */
		STATS;
	}

	/**
	 * Field size display parameter.
	 */
	private static final int FIELD_SIZE = 20;

	/**
	 * Horizontal frame margin.
	 */
	private static final int HOR_MARGIN = 50;

	/**
	 * Default program icon shown in title bar.
	 */
	private static final String PROGRAM_ICON = "./media/icon.png";

	/**
	 * Vertical frame margin.
	 */
	private static final int VERT_MARGIN = 80;

	/**
	 * Constructor with BlockingQueue referenced from controller.
	 * 
	 * @param blockingQueue
	 *            Referenced queue from controller
	 */
	public View (final BlockingQueue<SaperEvent> blockingQueue)
	{
		this.eventQueue = blockingQueue;
		initGUI();
	}

	/**
	 * Trigger window event => exit the game.
	 */
	public final void close ()
	{
		frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * Updates board and mine counter.
	 * 
	 * @param modelBoard
	 *            Board with fields get from Model.
	 * @param minesLeft
	 *            Number of mines left unflagged.
	 */
	public final void draw (final Field[][] modelBoard, final int minesLeft)
	{
		try
		{
			// Try updating board and counter with given data.
			EventQueue.invokeAndWait(new Runnable()
			{
				@Override
				public void run ()
				{
					board.draw(modelBoard, minesLeft);
					board.revalidate();

				}
			});
		}
		catch (InvocationTargetException | InterruptedException e)
		{
			JOptionPane.showMessageDialog(null, "Updating board failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Stops GameInfo's timer via Board and gets the time.
	 * 
	 * @return Time elapsed in seconds.
	 */
	public final int endGame ()
	{
		return board.endGame();
	}

	/**
	 * Displays or hides panels.
	 * 
	 * @param panel
	 *            Panel to be handled.
	 * @param isVisible
	 *            True for panel showing, false for hiding.
	 */
	public final void managePanels (final PanelTypes panel, final boolean isVisible)
	{
		// If not showing window with "about" info, remove all panels.
		if (panel != PanelTypes.ABOUT)
		{
			frame.remove(options);
			frame.remove(stats);
			frame.remove(board);
			frame.remove(help);
		}

		// If board needs to be shown.
		if (!isVisible || panel == PanelTypes.BOARD)
			frame.add(board);
		else
		{
			// Board is removed, show another panel.
			switch (panel)
			{
				case OPTIONS:
					frame.add(options);
					break;
				case STATS:
					frame.add(stats);
					break;
				case HELP:
					frame.add(help);
					break;
				case ABOUT:
					JOptionPane.showMessageDialog(frame, "Saper PROZ project.\nCurrent version: " + Saper.VERSION + "\n\nAll characters appearing in this work are fictitious.\nAny resemblance to real persons, living or dead, is purely coincidental.", "About", JOptionPane.INFORMATION_MESSAGE);
				default:
					break;
			}
		}

		// Correct display.
		frame.revalidate();
		frame.repaint();
	}

	/**
	 * Initialize new Board and sets new Window position.
	 */
	public final void newGame ()
	{
		// Draw board with fields.
		if (board != null)
			frame.remove(board);
		board = new Board(eventQueue);
		frame.add(board, BorderLayout.CENTER);

		// Show Board.
		managePanels(PanelTypes.BOARD, true);

		// Set new Window position.
		frame.setSize(Parameters.getWidth() * FIELD_SIZE + HOR_MARGIN * 2, Parameters.getHeight() * FIELD_SIZE + VERT_MARGIN * 2);
		frame.setLocation(new Point(width / 2 - frame.getSize().width / 2, height / 2 - frame.getSize().height / 2));

		// Acquire focus (for KeyListener).
		frame.requestFocusInWindow();

		// Correct display.
		frame.revalidate();
		frame.repaint();
	}

	/**
	 * Updated Stas with given data.
	 * 
	 * @param stats
	 *            GameStats with all data to be updated.
	 */
	public final void updateStats (GameStats stats)
	{
		this.stats.updateStats(stats);
	}

	/**
	 * Check the display for aspect ratio (screen resolution)
	 */
	private void getAspectRatio ()
	{
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		height = screenSize.height;
		width = screenSize.width;
	}

	/**
	 * Initialize graphical interface in separate thread.
	 */
	private final void initGUI ()
	{
		try
		{
			EventQueue.invokeAndWait(new Runnable()
			{
				public void run ()
				{
					try
					{
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}

					// Specify all frame parameters including window size, title, icon and position.
					getAspectRatio();
					frame = new JFrame();
					frame.setSize(Parameters.getWidth() * FIELD_SIZE + HOR_MARGIN * 2, Parameters.getHeight() * FIELD_SIZE + VERT_MARGIN * 2);
					frame.setLocation(new Point(width / 2 - frame.getSize().width / 2, height / 2 - frame.getSize().height / 2));
					frame.setTitle("Saper");
					frame.setResizable(false);
					frame.setIconImage(new ImageIcon(PROGRAM_ICON).getImage());

					// Set game menu bar.
					menu = new SaperMenu(eventQueue);
					frame.setJMenuBar(menu.getMenuBar());

					// Set options panel.
					options = new Options(eventQueue);

					// Set stats panel.
					stats = new Stats();

					// Set help panel.
					help = new Help();

					// Set board and others.
					newGame();

					// Set key listener.
					keyListener = new KeyListener()
					{
						@Override
						public void keyPressed (KeyEvent key)
						{
							// If Esc is hit, show board.
							if (key.getKeyCode() == KeyEvent.VK_ESCAPE)
								managePanels(PanelTypes.BOARD, true);
						}

						@Override
						public void keyReleased (KeyEvent key)
						{
							// DO NOTHING
						}

						@Override
						public void keyTyped (KeyEvent key)
						{
							// DO NOTHING
						}
					};

					frame.setFocusable(true);
					frame.requestFocusInWindow();
					frame.addKeyListener(keyListener);

					// Specify exit action and display the frame.
					frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
					frame.setVisible(true);

					// Custom listener with confirmation in case of exiting.
					frame.addWindowListener(new WindowAdapter()
					{
						public void windowClosing (WindowEvent e)
						{
							JFrame frame = (JFrame) e.getSource();

							int result =
							        JOptionPane.showConfirmDialog(frame, "Are you sure you want to exit the application?", "Exit Application", JOptionPane.YES_NO_OPTION);

							if (result == JOptionPane.YES_OPTION)
								frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						}
					});
				}
			});
		}
		catch (InvocationTargetException e)
		{
			JOptionPane.showMessageDialog(null, "Displaying frame failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
		}
		catch (InterruptedException e)
		{
			JOptionPane.showMessageDialog(null, "Displaying frame failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Displays fields, timer and mine counter as panel inside a frame.
	 */
	private Board board;

	/**
	 * Container for queueing special game events.
	 */
	private final BlockingQueue<SaperEvent> eventQueue;

	/**
	 * Main game frame (main window).
	 */
	private JFrame frame;

	/**
	 * Screen height in pixels.
	 */
	private int height;

	/**
	 * Help panel with general information and instructions.
	 * 
	 * @see saper.view.Help
	 */
	private Help help;

	/**
	 * Custom key listener with event for 'ESC' key hitting.
	 */
	private KeyListener keyListener;

	/**
	 * Drop-down menu with several options.
	 * 
	 * @see saper.view.SaperMenu
	 */
	private SaperMenu menu;

	/**
	 * Game options with difficulty switching.
	 * 
	 * @see saper.view.Options
	 */
	private Options options;

	/**
	 * Game statisctics.
	 * 
	 * @see saper.view.Stats
	 */
	private Stats stats;

	/**
	 * Screen width in pixels.
	 */
	private int width;
}
