package saper.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.BlockingQueue;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import saper.events.FieldEvent;
import saper.events.SaperEvent;
import saper.model.Field;
import saper.model.Parameters;

/**
 * @author Pawe� Kaczy�ski
 *         Panel with fields, timer and mine counter.
 */
public class Board extends JPanel
{
    /**
     * Implements events handling clicking on a field.
     */
    private class RevealAction implements MouseListener
    {
        /**
         * Constructor with button position and flagged argument.
         * 
         * @param btnPosition
         *            x-coordinate * maxHeight + y-coordinate
         * @param flagged
         *            Field was flagged or not.
         */
        public RevealAction (final int btnPosition, final boolean flagged)
        {
            this.btnPosition = btnPosition;
            this.flagged = flagged;
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseClicked (MouseEvent e)
        {
            // Acquire command.
            JButton button = (JButton) e.getComponent();
            command = button.getActionCommand();
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseEntered (MouseEvent e)
        {
            // Update graphics.
            JButton button = (JButton) e.getComponent();
            if (flagged)
                button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "flag_over.png"));
            else
                button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "field_over.png"));
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseExited (MouseEvent e)
        {
            // Update graphics.
            JButton button = (JButton) e.getComponent();
            if (flagged)
                button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "flag.png"));
            else
                button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "field.png"));
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
         */
        @Override
        public void mousePressed (MouseEvent e)
        {
            // Update grapchics.
            JButton button = (JButton) e.getComponent();
            if (flagged)
                button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "flag_pressed.png"));
            else
                button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "field_pressed.png"));

            // Create new events. Left click...
            if (e.getButton() == MouseEvent.BUTTON1 && !flagged)
                eventQueue.add(new FieldEvent(btnPosition, true));
            // ...or right click.
            else if (e.getButton() == MouseEvent.BUTTON3)
                eventQueue.add(new FieldEvent(btnPosition, false));
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseReleased (MouseEvent e)
        {
            // Check for command confirmation.
            JButton button = (JButton) e.getComponent();
            if (command == button.getActionCommand())
            {
                // Update graphics.
                if (flagged && e.getButton() != MouseEvent.BUTTON1)
                    button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "field.png"));
                else
                    button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "flag.png"));
            }
        }

        /**
         * X-coordinate * maxHeight + Y-coordinate
         */
        private final int btnPosition;

        /**
         * Command associated with field.
         */
        private String command = "";

        /**
         * Was this field flagged or not.
         */
        private final boolean flagged;
    }

    /**
     * Path to image files for fields.
     */
    private static final String FIELD_ICONS_PATH = "./media/fields/";

    /**
     * Path to media directory.
     */
    public static final String MEDIA_PATH = "./media/";

    /**
     * Board's ID for serialization.
     */
    private static final long serialVersionUID = 4L;

    /**
     * Constructor with BlockingQueue<SaperEvent>
     * 
     * @param blockingQueue
     *            Event queue referenced from View.
     */
    public Board (final BlockingQueue<SaperEvent> blockingQueue)
    {
        this.eventQueue = blockingQueue;

        // Create new panel with fields.
        fieldPanel = new JPanel();
        fieldPanel.setLayout(new GridLayout(Parameters.getHeight(), Parameters.getWidth(), -1, -1));
        fieldPanel.setBorder(new LineBorder(Color.BLACK, 2, false));
        fieldPanel.setBackground(Color.GRAY);

        // Create fields.
        for (int i = 0; i < Parameters.getHeight(); i++)
            for (int k = 0; k < Parameters.getWidth(); k++)
                addBtn(k * Parameters.getHeight() + i, false);

        // Set default background.
        background = new ImageIcon("./media/background.png").getImage();

        // Extra margin.
        JLabel label = new JLabel(".......................");
        label.setFont(label.getFont().deriveFont(30f));
        label.setForeground(new Color(0, 0, 0, 0));

        // Add field panel.
        add(label, BorderLayout.NORTH);
        add(fieldPanel);

        // Create and add new game info (time and counter).
        gameInfo = new GameInfo();
        add(gameInfo);

    }

    /**
     * Updates fields from given array from model.
     * 
     * @param board
     *            Fields data to be updated from.
     * @param minesLeft
     *            Number of mines left on the board unflagged.
     */
    public final void draw (final Field[][] board, final int minesLeft)
    {
        // Remove all fields.
        fieldPanel.removeAll();
        fieldPanel.setLayout(new GridLayout(Parameters.getHeight(), Parameters.getWidth(), -1, -1));

        try
        {
            // Insert new fields.
            for (int i = 0; i < Parameters.getHeight(); i++)
                for (int k = 0; k < Parameters.getWidth(); k++)
                    if (board[k][i].getIsVisible())
                        addImage(board[k][i]);
                    else
                        addBtn(k * Parameters.getHeight() + i, board[k][i].getFlagged());
        }
        catch (NullPointerException e)
        {
            JOptionPane.showMessageDialog(null, "Drawing board failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            JOptionPane.showMessageDialog(null, "Drawing board failed!\n" + e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
        }

        // Update mines counter.
        gameInfo.updateMines(minesLeft);
    }

    /**
     * Stops the timer.
     * 
     * @return Number of second elapsed.
     */
    public final int endGame ()
    {
        return gameInfo.getGameTime();
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     * Draws background.
     */
    public void paintComponent (Graphics g)
    {
        g.drawImage(background, 0, 0, null);
    }

    /**
     * Add button at current position and sets flag if necessary.
     * 
     * @param position
     *            Button position.
     * @param isFlagged
     *            Is button flagged or not.
     */
    private void addBtn (final int position, final boolean isFlagged)
    {
        // Create new button.
        JButton button = new JButton();
        button.setMargin(new Insets(-2, -2, -2, -2));

        // Choose img file
        if (isFlagged)
            button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "flag.png"));
        else
            button.setIcon(new ImageIcon(FIELD_ICONS_PATH + "field.png"));

        // Set button parameters.
        button.setContentAreaFilled(false);
        button.setRolloverEnabled(true);
        button.setFocusable(false);
        String command = "";
        button.setActionCommand(command + position);

        // Add listener.
        RevealAction listener = new RevealAction(position, isFlagged);
        button.addMouseListener(listener);

        fieldPanel.add(button);
    }

    /**
     * Adds revealed fields (no listener and clicked/rollover actions).
     * 
     * @param field
     *            Field to be represented on the board.
     */
    private void addImage (final Field field)
    {
        // Acquire img. file
        String filePath = FIELD_ICONS_PATH + field.getType().getImgFile();

        // Create new button with specific parameters.
        JButton button = new JButton();
        button.setMargin(new Insets(-2, -2, -2, -2));
        button.setIcon(new ImageIcon(filePath));
        button.setContentAreaFilled(false);
        button.setFocusable(false);

        fieldPanel.add(button);
    }

    /**
     * Default background for panel.
     */
    private Image background;

    /**
     * Shared event queue.
     */
    private final BlockingQueue<SaperEvent> eventQueue;

    /**
     * Panel with grid with fields.
     */
    private JPanel fieldPanel;

    /**
     * Mine counter and timer.
     */
    private GameInfo gameInfo;
}
