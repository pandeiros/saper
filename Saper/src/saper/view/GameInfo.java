package saper.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * @author Pawe� Kaczy�ski
 *         Implements timer for counting seconds elapsed and mine counter showing how many more
 *         mines are not flagged.
 */
public class GameInfo extends JPanel
{
    /**
     * Implements increasing displayed time after timer interval.
     */
    private class TimerAction implements ActionListener
    {
        /*
         * (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed (ActionEvent event)
        {
            // Increment seconds and update label.
            gameTime++;
            timeLabel.setText(gameTime + "");
        }

    }

    /**
     * GameInfo's ID for serialization.
     */
    private static final long serialVersionUID = 5L;

    /**
     * Default constructor.
     */
    public GameInfo ()
    {
        // Set panel.
        this.setLayout(new GridLayout(1, 3, 20, 20));
        this.setBackground(new Color(150, 218, 253, 255));

        // Set time label.
        timeLabel = new JLabel();
        timeLabel.setText(gameTime + "");
        timeLabel.setSize(100, 50);
        timeLabel.setFont(timeLabel.getFont().deriveFont(30f));
        timeLabel.setForeground(new Color(50, 50, 50, 255));
        timeLabel.setIcon(new ImageIcon(Board.MEDIA_PATH + "timer.png"));

        // Set mines counter label.
        minesLabel = new JLabel();
        minesLabel.setText(gameTime + "");
        minesLabel.setSize(100, 50);
        minesLabel.setFont(timeLabel.getFont().deriveFont(30f));
        minesLabel.setForeground(new Color(50, 50, 50, 255));
        minesLabel.setIcon(new ImageIcon(Board.MEDIA_PATH + "counter.png"));

        // Add components to the panel.
        add(timeLabel);
        add(minesLabel);

        // Set timer.
        timer = new Timer(1000, new TimerAction());
        timer.setRepeats(true);
    }

    /**
     * Stops the timer and returns seconds elapsed.
     * 
     * @return Time elapsed (in seconds).
     */
    public final int getGameTime ()
    {
        timer.stop();
        return gameTime;
    }

    /**
     * Update mines left counter. Additionaly starts the timer, if necessary.
     * 
     * @param minesLeft
     *            Current value of mines left on the board.
     */
    public final void updateMines (final int minesLeft)
    {
        // Starts timer if not running.
        if (!timer.isRunning())
            timer.start();

        // Update counter.
        minesLabel.setText(minesLeft + "");
    }

    /**
     * Current game time in seconds.
     */
    private int gameTime = 0;

    /**
     * Label for displaying mines left.
     */
    private JLabel minesLabel;

    /**
     * Label for displaying time.
     */
    private JLabel timeLabel;

    /**
     * Timer for counting seconds
     */
    private Timer timer;
}
