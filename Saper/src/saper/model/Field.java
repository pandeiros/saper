package saper.model;

import java.io.Serializable;

/**
 * @author Pawe� Kaczy�ski
 *         Representation of one field in the game. It is defined by FieldType and current
 *         visibility.
 */
public class Field implements Serializable
{
    /**
     * Contains of all available field type with their numerical value and associated path to image
     * file.
     */
    public static enum FieldType
    {
        /**
         * Field type for empty field (no adjacent mines).
         */
        EMPTY (0, "empty.png"),

        /**
         * Field type for field with 1 adjacent mine.
         */
        M1 (1, "m1.png"),

        /**
         * Field type for field with 2 adjacent mines.
         */
        M2 (2, "m2.png"),

        /**
         * Field type for field with 3 adjacent mines.
         */
        M3 (3, "m3.png"),

        /**
         * Field type for field with 4 adjacent mines.
         */
        M4 (4, "m4.png"),

        /**
         * Field type for field with 5 adjacent mines.
         */
        M5 (5, "m5.png"),

        /**
         * Field type for field with 6 adjacent mines.
         */
        M6 (6, "m6.png"),

        /**
         * Field type for field with 7 adjacent mines.
         */
        M7 (7, "m7.png"),

        /**
         * Field type for field with 8 adjacent mines.
         */
        M8 (8, "m8.png"),

        /**
         * Field type for mine field.
         */
        MINE (9, "mine.png");

        /**
         * Static method for int to FieldType conversion.
         * 
         * @param value
         *            Value to be converted into associated FieldType.
         * @return FieldType with value given as argument.
         */
        public final static FieldType intToFieldType (final int value)
        {
            for (FieldType type : FieldType.values())
                if (type.value == value)
                    return type;

            return EMPTY;
        }

        /**
         * Constructor with type value and filepath to associated image.
         * 
         * @param value
         *            Type numerical value.
         * @param imgFile
         *            Path to associated file.
         */
        FieldType (final int value, final String imgFile)
        {
            this.value = value;
            this.imgFile = imgFile;
        }

        /**
         * Path to image file getter.
         * 
         * @return Path to the .img file of this FieldType object.
         */
        public final String getImgFile ()
        {
            return this.imgFile;
        }

        /**
         * Value getter.
         * 
         * @return Value of this FieldType object.
         */
        public final int getValue ()
        {
            return this.value;
        }

        /**
         * Path to associated .png file.
         */
        private final String imgFile;

        /**
         * Numerical value of particulate FieldType.
         */
        private final int value;

    }

    /**
     * Field's ID for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with FieldType.
     * 
     * @param type
     *            Type of which this Field should be.
     */
    Field (final FieldType type)
    {
        this.type = type;
    }

    /**
     * Flagged getter.
     * 
     * @return Whether field was flagged on the board or not.
     */
    public final boolean getFlagged ()
    {
        return flagged;
    }

    /**
     * Visibility getter.
     * 
     * @return Boolean value whether field is visible or not (already clicked/revealed or not).
     */
    public final boolean getIsVisible ()
    {
        return isVisible;
    }

    /**
     * Type getter.
     * 
     * @return Type of this field.
     */
    public final FieldType getType ()
    {
        return type;
    }

    /**
     * Visited getter.
     * 
     * @return Whether field was visited or not.
     */
    public final boolean getVisited ()
    {
        return visited;
    }

    /**
     * Flagged setter.
     * 
     * @param flagged
     *            Changes field's flagged parameter to given.
     */
    public final void setFlagged (final boolean flagged)
    {
        this.flagged = flagged;
    }

    /**
     * Visibility setter.
     * 
     * @param visibility
     *            Changes field's visibility to given.
     */
    public final void setIsVisible (final boolean visibility)
    {
        this.isVisible = visibility;
    }

    /**
     * Visited setter.
     * 
     * @param visited
     *            Changes field's visited member to given boolean value.
     */
    public final void setVisited (final boolean visited)
    {
        this.visited = visited;
    }

    /**
     * Is this field flagged on the board or not.
     */
    private boolean flagged = false;

    /**
     * Tells if field is visible or not (was already clicked/revealed or not).
     */
    private boolean isVisible = false;

    /**
     * Binded FieldType to the field.
     * 
     * @see saper.model.Field.FieldType
     */
    private final FieldType type;

    /**
     * Additional field for recursive algorithm telling whether this field was visited or not.
     */
    private boolean visited = false;
}
