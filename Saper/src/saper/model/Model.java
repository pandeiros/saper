package saper.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import saper.model.Field.FieldType;

/**
 * @author Pawe� Kaczy�ski
 *         Model for project "Saper". Consists of 2-dimensional array 'board' representing fields
 *         layout. Additionally provides all operations on fields, especially board initialization
 *         and fields revealing.
 */
public class Model implements Serializable
{
    /**
     * Model's ID for serialization.
     */
    private static final long serialVersionUID = 3L;

    /**
     * Default constructor with board creation based on Parameters class.
     */
    public Model ()
    {
        board = new Field[Parameters.getWidth()][Parameters.getHeight()];
        minesLeft = Parameters.getMines();
        difficulty = Parameters.difficulty;
    }

    /**
     * Board getter.
     * 
     * @return Representation of all fields on the board.
     * @see saper.model.Model#board
     */
    public final Field[][] getBoard ()
    {
        return board;
    }

    /**
     * Difficulty getter.
     * 
     * @return Difficulty associated with this model.
     */
    public final Parameters.Difficulty getDifficulty ()
    {
        return difficulty;
    }

    /**
     * Gets the number of currently revealed fields on the board.
     * 
     * @return Number of revealed fields.
     */
    public final int getFieldsRevealed ()
    {
        return fieldsRevealed;
    }

    /**
     * Gets the information whether board was already initialized.
     * 
     * @return True if board was initialized, false otherwise.
     */
    public final boolean getInitialized ()
    {
        return initialized;
    }

    /**
     * Checker for losing condition.
     * 
     * @return True if mine has been clicked, false otherwise.
     */
    public final boolean getMineRevealed ()
    {
        return mineRevealed;
    }

    /**
     * Counts all mines.
     * 
     * @return Number of all mines on the current board.
     */
    public final int getMines ()
    {
        int suma = 0;

        // Iterate through 2-dimensional array.
        for (Field[] x : board)
            for (Field y : x)
                if (y.getType() == Field.FieldType.MINE)
                    suma++;

        return suma;
    }

    /**
     * Gets the number of mines left on the board.
     * 
     * @return Number of mines left.
     */
    public final int getMinesLeft ()
    {
        return minesLeft;
    }

    /**
     * PendingFields getter. Resets the value to 0 as well.
     * 
     * @return Number of fields revealed that have not yet been saved in stats.
     */
    public final int getPendingFieldsRevealed ()
    {
        int pending = pendingFieldsRevealed;
        pendingFieldsRevealed = 0;
        return pending;
    }

    /**
     * Controls the game for winning or losing conditions.
     * 
     * @return True if the game is still in progress, false if the game is finished.
     */
    public final boolean getStatus ()
    {
        // Condition for game to be still in progress.
        if (fieldsRevealed + Parameters.getMines() >= Parameters.getHeight() * Parameters.getWidth() && minesLeft == 0 || mineRevealed)
            return false;
        else
            return true;
    }

    /**
     * Initiates mine positions on the board randomly, but also takes into consideration user's
     * first click. The idea is to prevent from clicking the mine on the first try when the game
     * starts.
     */
    public final void initiateBoard (final int x, final int y)
    {
        initialized = true;

        // Set of mine coordinates (contains values = x * height + y, therefore they are unique).
        Set<Integer> mines = new HashSet<Integer>(Parameters.getMines());

        // Randomly generate mines positions and insert them to the set.
        while (mines.size() < Parameters.getMines())
        {
            int randX = (int) (Math.random() * Parameters.getWidth());
            int randY = (int) (Math.random() * Parameters.getHeight());

            // If position of mine to be set is near the pressed position, generate it again.
            if (Parameters.firstClickSafe)
                if (randX >= x - 1 && randX <= x + 1 && randY >= y - 1 && randY <= y + 1)
                    continue;

            int newMine = randX * Parameters.getHeight() + randY;

            mines.add(newMine);
        }

        // Create Field with type MINE on the board.
        for (Integer mine : mines)
        {
            int posX = mine / Parameters.getHeight();
            int posY = mine % Parameters.getHeight();
            board[posX][posY] = new Field(FieldType.MINE);
        }

        // Create the rest of the fields. For each fiels calculate the adjacent mines
        for (int i = 0; i < Parameters.getWidth(); ++i)
            for (int k = 0; k < Parameters.getHeight(); ++k)
                if (board[i][k] == null) // If not yet initialized.
                    board[i][k] = new Field(acquireFieldType(i, k));
        return;
    }

    /**
     * Sets all fields to 'not visited' after revealing recursive algorithm ends.
     */
    public final void resetAllVisited ()
    {
        // Iterate through the whole board.
        for (int i = 0; i < Parameters.getWidth(); i++)
            for (int k = 0; k < Parameters.getHeight(); k++)
                if (Parameters.checkCoordinates(i, k))
                    board[i][k].setVisited(false);
    }

    /**
     * Makes pressed field visible and initiate recursive unvealing of adjecent fields if necessary.
     * 
     * @param x
     *            Field column number
     * @param y
     *            Field row number
     * @return False if revealed field hid mine. True instead.
     */
    public final boolean revealPressedField (final int x, final int y)
    {
        // Reveal the field.
        if (Parameters.checkCoordinates(x, y) && !board[x][y].getIsVisible())
        {
            board[x][y].setIsVisible(true);

            // Check for mine.
            if (board[x][y].getType() == FieldType.MINE)
            {
                setAllMinesVisible();
                mineRevealed = true;
            }

            // Recursively reveal other fields if necessary
            if (board[x][y].getType() == FieldType.EMPTY)
                revealField(x, y);
            else
                fieldsRevealed++;

            // Reset visited variable of every field.
            resetAllVisited();
        }

        return true;
    }

    /**
     * After failure, all mines need to be revealed.
     */
    public final void setAllMinesVisible ()
    {
        // Iterate through board, reveal all mines.
        for (int i = 0; i < Parameters.getWidth(); i++)
            for (int k = 0; k < Parameters.getHeight(); k++)
                if (Parameters.checkCoordinates(i, k))
                    if (board[i][k].getType() == FieldType.MINE)
                        board[i][k].setIsVisible(true);
    }

    /**
     * Fields revealed setter.
     * 
     * @param fieldsRevealed
     *            New number of fieldsRevealed to be set.
     */
    public final void setFieldsRevealed (final int fieldsRevealed)
    {
        this.fieldsRevealed = fieldsRevealed;
    }

    /**
     * Adds or removes mines left counter.
     * 
     * @param increment
     *            True for incrementing minesLeft, false for decrementing.
     */
    public final void updateMinesLeft (final boolean increment)
    {
        if (!increment && minesLeft > 0)
            minesLeft--;
        else if (minesLeft < Parameters.getMines())
            minesLeft++;
    }

    /**
     * Acquires field type by its coordinates and depending on its neighbors.
     * 
     * @param x
     *            Column (x coordinate) of the field.
     * @param y
     *            Row (y coordinate) of the field.
     * @return Type of the field in the (x, y) coordinates.
     */
    private final FieldType acquireFieldType (final int x, final int y)
    {
        // Count mine and return type accordingly to it.
        int mineCount = checkAdjacentMines(x, y);
        return FieldType.intToFieldType(mineCount);
    }

    /**
     * Check all 8 surrounding field (if they exist) and count encountered mines.
     * 
     * @param x
     *            Column (x coordinate) of the field.
     * @param y
     *            Row (y coordinate) of the field.
     * @return Mine count (value of the field).
     */
    private final int checkAdjacentMines (final int x, final int y)
    {
        int mines = 0;

        // Check every neighbor whether it is a mine or not and summ it all up.
        for (int i = x - 1; i <= x + 1; i++)
            for (int k = y - 1; k <= y + 1; k++)
                if (Parameters.checkCoordinates(i, k))
                    if (board[i][k] != null && board[i][k].getType() == FieldType.MINE)
                        mines++;

        // Return mine count.
        return mines;
    }

    /**
     * Reveals all available fields starting from given coordinates.
     * Revealing algorithm is pretty simple: You only enter blank fields and reveal all surronding
     * field, except for mines. Additionally check for visited fields is necessary.
     * 
     * @param x
     *            Column number
     * @param y
     *            Row number
     */
    private final void revealField (final int x, final int y)
    {
        // Check if field needs futher attention.
        if (Parameters.checkCoordinates(x, y) && !board[x][y].getVisited())
        {
            // If field is not a mine.
            if (board[x][y].getType() != FieldType.MINE)
            {
                // Reveal this one and mark that it was visited.
                board[x][y].setIsVisible(true);
                board[x][y].setVisited(true);
                fieldsRevealed++;
                pendingFieldsRevealed++;

                // Try to reveal all adjacent fields.
                if (board[x][y].getType() == FieldType.EMPTY)
                    for (int i = x - 1; i <= x + 1; i++)
                        for (int k = y - 1; k <= y + 1; k++)
                            revealField(i, k);
            }
            else
                return;
        }
    }

    /**
     * Representation of fields. First index is column, the second is row. For example:
     * board [x][y] id referencing to the field in (x, y) position.
     */
    private final Field[][] board;

    /**
     * Difficulty associated with model.
     */
    private final Parameters.Difficulty difficulty;

    /**
     * Number of currently visible fields on the board.
     */
    private int fieldsRevealed = 0;

    /**
     * Information for controller to check that board was initialized before any actions or to
     * initialize it only after first click.
     */
    private boolean initialized = false;

    /**
     * True if mine was clicked, false otherwise.
     */
    private boolean mineRevealed = false;

    /**
     * Counter of mines left on the board unflagged.
     */
    private int minesLeft;

    /**
     * Number of fields revealed that have not yet been saved in stats.
     */
    private int pendingFieldsRevealed = 0;
}
