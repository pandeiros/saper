package saper.model;

/**
 * @author Pawe� Kaczy�ski
 *         Class defining game options, board size and mines displacement. All field are static, no
 *         instace needed.
 */
public final class Parameters
{
    /**
     * Contains of all available game difficulties.
     */
    public enum Difficulty
    {
        /**
         * Custom difficulty. Player specifies width, height and mine number.
         */
        CUSTOM (0),

        /**
         * Easy difficulty (9 x 9, 10 mines).
         */
        EASY (1),

        /**
         * Hard difficulty (24 x 20, 100 mines).
         */
        HARD (3),

        /**
         * Medium difficulty (16 x 16, 40 mines).
         */
        MEDIUM (2);

        /**
         * Difficulty initializer.
         * 
         * @param value
         *            Numerical value associated with particular difficulty.
         */
        Difficulty (final int value)
        {
            this.value = value;
        }

        /**
         * Difficulty numerical value getter.
         * 
         * @return Numerical value associated with particular difficulty.
         */
        public final int getValue ()
        {
            return this.value;
        }

        /**
         * Numerical value associated with particular difficulty.
         */
        private final int value;
    }

    /**
     * Contains all available messages for this class' methods to return. Every single message has
     * it's integer flag and String text associated with.
     */
    public enum Errors
    {
        /**
         * Message for correct board size and mines number.
         */
        BOARD_CORRECT (0, "Board data correct."),

        /**
         * Message for board's height value excess.
         */
        BOARD_HEIGHT_TOO_LONG (1 << 2, "Board height is to big! Insert height from given range."),

        /**
         * Message for board's height value insufficiency.
         */
        BOARD_HEIGHT_TOO_SHORT (1 << 3, "Board height is to small! Insert height from given range."),

        /**
         * Message for board's mines number value exceeding accordingly to given width and height.
         */
        BOARD_MINES_NOT_ENOUGH_SPACE (1 << 6, "Mine number is too big according to board size. This number cannot be bigger than " + (int) (MINES_MULTIPLIER * 100) + "% of all fields available)"),

        /**
         * Message for board's mines number value insufficiency.
         */
        BOARD_MINES_TOO_LESS (1 << 5, "Mine number is too small! Insert mine number from given range."),

        /**
         * Message for board's mines number value excess.
         */
        BOARD_MINES_TOO_MANY (1 << 4, "Mine number is too big! Insert mine number from given range."),

        /**
         * Message for board's width value excess.
         */
        BOARD_WIDTH_TOO_LONG (1, "Board width is to big! Insert width from given range."),

        /**
         * Message for board's width value insufficiency.
         */
        BOARD_WIDTH_TOO_SHORT (1 << 1, "Board width is to small! Insert width from given range.");

        /**
         * Constructor with integer flag and text message associated with enumerator.
         */
        Errors (final int value, final String message)
        {
            this.value = value;
            this.message = message;
        }

        /**
         * Message getter.
         * 
         * @return Text message associated with the error.
         */
        public final String getMessage ()
        {
            return message;
        }

        /**
         * Value getter.
         * 
         * @return Integer flag associated with the error.
         */
        public final int getValue ()
        {
            return value;
        }

        /**
         * Error's text message.
         */
        private final String message;

        /**
         * Error's numerical flag.
         */
        private final int value;
    }

    /**
     * Defines maximum part of all fields for mines.
     */
    public final static float MINES_MULTIPLIER = 0.9f;

    /**
     * Board height maximum limit.
     */
    public final static int MAX_HEIGHT = 24;

    /**
     * Board width maximum limit.
     */
    public final static int MAX_WIDTH = 30;

    /**
     * Board mines number maximum limit.
     */
    public final static int MAX_MINES = (int) (MAX_WIDTH * MAX_HEIGHT * MINES_MULTIPLIER);

    /**
     * Board mines number minimum limit.
     */
    public final static int MIN_MINES = 10;

    /**
     * Board width and height minimum limit.
     */
    public final static int MIN_SIZE = 9;

    /**
     * Current game difficulty
     */
    public static Difficulty difficulty = Difficulty.EASY;

    /**
     * If true, the very first attepmt to reveal a field will be secured from clicking a mine.
     */
    public static boolean firstClickSafe = true;

    /**
     * Board height parameter.
     */
    private static int height;

    /**
     * Board mine number parameter.
     */
    private static int mines;

    /**
     * True if parameters need reinitialization, false otherwise.
     */
    private static boolean reset = false;

    /**
     * Board width parameter.
     */
    private static int width;

    /**
     * Checks if given arguments describing board are correct.
     * 
     * @param width
     *            Width of the board.
     * @param height
     *            Height of the board.
     * @param mines
     *            Number of mines on the board.
     * @return One of the Errors messages.
     */
    public final static Errors checkBoardData (final int width, final int height, final int mines)
    {
        // Check limits.
        if (width < MIN_SIZE)
            return Errors.BOARD_WIDTH_TOO_SHORT;
        if (width > MAX_WIDTH)
            return Errors.BOARD_WIDTH_TOO_LONG;

        if (height < MIN_SIZE)
            return Errors.BOARD_HEIGHT_TOO_SHORT;
        if (height > MAX_HEIGHT)
            return Errors.BOARD_HEIGHT_TOO_LONG;

        if (mines < MIN_MINES)
            return Errors.BOARD_MINES_TOO_LESS;
        if (mines > MAX_MINES)
            return Errors.BOARD_MINES_TOO_MANY;

        // Check for enough space for mines.
        int countMinesAvailable = (int) (width * height * MINES_MULTIPLIER);
        if (mines > countMinesAvailable)
            return Errors.BOARD_MINES_NOT_ENOUGH_SPACE;

        // Everything's allright.
        return Errors.BOARD_CORRECT;
    }

    /**
     * Safe checker for coordinates.
     * 
     * @param x
     *            x-coordinate (column)
     * @param y
     *            y-coordinate (row)
     * @return True of coordinates match field on the board.
     */
    public static final boolean checkCoordinates (final int x, final int y)
    {
        // Check limits.
        if (x >= 0 && x < Parameters.width && y >= 0 && y < Parameters.height)
            return true;
        else
            return false;
    }

    /**
     * Height getter.
     * 
     * @return Board height.
     */
    public static final int getHeight ()
    {
        return height;
    }

    /**
     * Gets the text message associated with the error given.
     * 
     * @param error
     *            Error for message to be retrieved.
     * @return Message associated with error.
     */
    public final static String getMessage (final Errors error)
    {
        return error.getMessage();
    }

    /**
     * Mines getter.
     * 
     * @return Number of mines on the board.
     */
    public static final int getMines ()
    {
        return mines;
    }

    /**
     * Reset value getter.
     * 
     * @return True if class Parameters needs to be reinitialized.
     */
    public static final boolean getReset ()
    {
        return Parameters.reset;
    }

    /**
     * Width getter.
     * 
     * @return Width of the board.
     */
    public static final int getWidth ()
    {
        return width;
    }

    /**
     * Method for EASY, MEDIUM and HARD difficulties initialization.
     * 
     * @param difficulty
     *            One of EASY, MEDIUM or HARD difficulty on which special preset is loaded.
     * @return Boolean value if operation was succesful (true) or not (false).
     */
    public static final boolean setParameters (final Difficulty difficulty)
    {
        // Prevents from loading CUTOM difficulty without board details.
        if (difficulty == Difficulty.CUSTOM)
            return false;

        // Set current difficulty.
        Parameters.difficulty = difficulty;

        // Set values from presets.
        switch (difficulty)
        {
            case EASY:
                width = 9;
                height = 9;
                mines = 10;
                break;
            case MEDIUM:
                width = 16;
                height = 16;
                mines = 40;
                break;
            case HARD:
                width = 24;
                height = 20;
                mines = 100;
                break;
            default:
                break;
        }

        return true;
    }

    /**
     * Method for CUSTOM difficulty initialization.
     * 
     * @param difficulty
     *            Difficulty to be loaded. If CUTOM is not given, calls
     *            Parameter.setParameters(Difficulty) method instead.
     * @param width
     *            Number of columns.
     * @param height
     *            Number of rows.
     * @param mines
     *            Number of mines.
     * @return Boolean value if operation was succesful (true) or not (false).
     */
    public static final boolean setParameters (final Difficulty difficulty, final int width, final int height, final int mines)
    {
        // If not CUSTOM, call another method.
        if (difficulty != Difficulty.CUSTOM)
            return Parameters.setParameters(difficulty);

        // Check limits.
        if (width < MIN_SIZE || width > MAX_WIDTH)
            return false;

        if (height < MIN_SIZE || height > MAX_HEIGHT)
            return false;

        if (mines < MIN_MINES || mines > MAX_MINES)
            return false;

        // Set values.
        Parameters.difficulty = difficulty;
        Parameters.width = width;
        Parameters.height = height;
        Parameters.mines = mines;

        return true;
    }

    /**
     * Set Parameters values by type given as String.
     * 
     * @param difficulty
     *            String name of difficulty.
     * @return Boolean value if operation was succesful (true) or not (false).
     */
    public static final boolean setParameters (final String difficulty)
    {
        // Prevents from loading CUTOM difficulty without board details.
        if (difficulty == "CUSTOM")
            return false;

        // Load presets.
        if (difficulty == "EASY")
            setParameters(Difficulty.EASY);
        else if (difficulty == "MEDIUM")
            setParameters(Difficulty.MEDIUM);
        else if (difficulty == "HARD")
            setParameters(Difficulty.HARD);

        return true;
    }

    /**
     * Reste setter.
     * 
     * @param reset
     *            True for need for reset, false otherwise.
     */
    public static final void setReset (final boolean reset)
    {
        Parameters.reset = reset;
    }
}
