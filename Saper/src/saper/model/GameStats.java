package saper.model;

import java.io.Serializable;

/**
 * @author Pawe� Kaczy�ski
 *         Holds global game progress (number of games, wins percentage and other).
 */
public class GameStats implements Serializable
{
    /**
     * GameStats' ID for serialization.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Default constructor.
     */
    public GameStats ()
    {
        this.games = 0;
        this.wins = 0;
        this.fieldsRevealed = 0;
        this.minesSecured = 0;
        this.flagSet = 0;
    }

    /**
     * Fields revealed getter.
     * 
     * @return Number of fields revealed ever.
     */
    public final int getFieldsRevealed ()
    {
        return fieldsRevealed;
    }

    /**
     * Flag set getter.
     * 
     * @return Number of set flags on the board ever.
     */
    public final int getFlagSet ()
    {
        return flagSet;
    }

    /**
     * Games counter getter.
     * 
     * @return Number of all games played.
     */
    public final int getGames ()
    {
        return games;
    }

    /**
     * Mines secured getter.
     * 
     * @return Number of mines secured in all time.
     */
    public final int getMinesSecured ()
    {
        return minesSecured;
    }

    /**
     * Wins getter.
     * 
     * @return Number of winned games.
     */
    public final int getWins ()
    {
        return wins;
    }

    /**
     * Field revealed incrementation.
     * 
     * @param value
     *            Value to be added to the current fieldRevealed value.
     */
    public void incFieldRevealed (final int value)
    {
        this.fieldsRevealed += value;
    }

    /**
     * Flag set incrementation.
     */
    public void incFlagSet ()
    {
        this.flagSet++;
    }

    /**
     * Games number incrementation.
     */
    public void incGames ()
    {
        this.games++;
    }

    /**
     * Mines secured incrementation. Always increment by current Parameters.mines value and only at
     * the end of the game.
     */
    public void incMinesSecured ()
    {
        this.minesSecured += Parameters.getMines();
    }

    /**
     * Wins incrementation.
     * Increment games as well.
     */
    public void incWins ()
    {
        this.wins++;
        this.games++;
    }

    /**
     * Sets all values to zero.
     */
    public final void reset ()
    {
        this.games = 0;
        this.wins = 0;
        this.fieldsRevealed = 0;
        this.minesSecured = 0;
        this.flagSet = 0;
    }

    /**
     * Number of all field revealed while playing games.
     */
    private int fieldsRevealed = 0;

    /**
     * Number of all flag set while playing games.
     */
    private int flagSet = 0;

    /**
     * Number of all games.
     */
    private int games = 0;

    /**
     * Number of all minesSecured.
     */
    private int minesSecured = 0;

    /**
     * Number or all victories.
     */
    private int wins = 0;
}
